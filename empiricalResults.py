#!/usr/bin/env python


#import paramsPredators
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
import datetime
from basicsModule import dwrpcauchy
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
import calendar

def logit(x):
    """
    Function (logit) to convert probability to real number
    """
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    """
    Function to convert real number to probability
    """
    return np.exp(x) / (1 + np.exp(x))

def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D


def add_months(sourcedate, months):
    month = sourcedate.month - 1 + months
    year = sourcedate.year + month // 12
    month = month % 12 + 1
    day = min(sourcedate.day, calendar.monthrange(year,month)[1])
    return datetime.date(year, month, day)

class ResultsProcessing(object):
    def __init__(self, gibbsobj, basicobj, params, pigpath):

        self.params = params
        self.possumpath = pigpath
        self.gibbsobj = gibbsobj
        self.basicobj = basicobj
        self.ndat = len(self.gibbsobj.sigmagibbs)
        self.results = np.hstack([self.gibbsobj.betagibbs,
                                np.expand_dims(self.gibbsobj.agibbs, 1),
                                np.expand_dims(self.gibbsobj.bgibbs, 1),
                                np.expand_dims(self.gibbsobj.mugibbs, 1),
                                np.expand_dims(self.gibbsobj.sigmagibbs, 1),
                                np.expand_dims(self.gibbsobj.nugibbs, 1),
                                np.expand_dims(self.gibbsobj.epsilgibbs, 1)])
        self.npara = self.results.shape[1]
        print('total iter', ((self.ndat * self.params.thinrate) + 
                self.params.burnin))
        print('thin = ', self.params.thinrate, 'burn = ', self.params.burnin)


    def makeTableFX(self):
        self.ncol = np.shape(self.results)[1]
        if self.params.modelID == 1:
            self.names = np.array(['PTus200', 'MnDevN200', 'D2Riv200', 'PFor200',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 2:
            self.names = np.array(['PTus200', 'MnDevN200', 'D2Riv200', 'PFor200',
                'PBog200', 'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 3:
            self.names = np.array(['PTus500', 'MnDevN500', 'D2Riv500', 'PFor500',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 4:
            self.names = np.array(['PTus500', 'MnDevN500', 'D2Riv500', 'PFor500',
                'PBog500', 'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 5:
            self.names = np.array(['PTus200', 'MnDevN200', 'D2Riv200',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 6:
            self.names = np.array(['MnDevN200', 'D2Riv200', 'PFor200',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 7:
            self.names = np.array(['PTus200', 'MnDevN200', 'D2Riv200', 'PBog200',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 8:
            self.names = np.array(['MnDevN200', 'D2Riv200', 'PFor200', 'PBog200',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 9:
            self.names = np.array(['PTus500', 'MnDevN500', 'D2Riv500',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 10:
            self.names = np.array(['MnDevN500', 'D2Riv500', 'PFor500',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 11:
            self.names = np.array(['PTus500', 'MnDevN500', 'D2Riv500', 'PBog500',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])
        if self.params.modelID == 12:
            self.names = np.array(['MnDevN500', 'D2Riv500', 'PFor500', 'PBog500',
                'a_wrpC', 'b_wrpC', 'mu', 'sigma', 'nu', 'epsilon'])

        resultTable = np.zeros(shape = (3, self.ncol))
        resultTable[0] = np.round(np.mean(self.results[:, :self.ncol], axis = 0), 3)
        resultTable[1:3] = np.round(mquantiles(self.results[:, :self.ncol], 
                prob=[0.05, 0.95], axis = 0), 3)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Names', 'Mean', 'Low CI', 'High CI'])
        for i in range(self.ncol):
            name = self.names[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print(aa)
        self.summaryTable = resultTable.copy()

    ########            Write data to file
    ########
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Names', 'U12'), ('Mean', np.float),
                    ('Low CI', np.float), ('High CI', np.float)])
        # copy data over
        structured['Mean'] = self.summaryTable[:, 0]
        structured['Low CI'] = self.summaryTable[:, 1]
        structured['High CI'] = self.summaryTable[:, 2]
        structured['Names'] = self.names
        np.savetxt(self.params.paramsResFname, structured, 
            fmt=['%s', '%.4f', '%.4f', '%.4f'],
                    comments = '', delimiter = ',', 
                    header='Names, Mean, Low_CI, High_CI')


   

    def tracePlotFX(self):
        """
        plot diagnostic trace plots
        """
        cc = 0
        plotNames = self.names
        plotArray = self.results
        nTotalFigs = self.ncol
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3,j+1)
                if cc < nTotalFigs:
                    P.plot(plotArray[:, cc])
                    P.title(plotNames[cc])
                cc += 1
            P.show(block=lastFigure)

    def traceAlpha0Plot(self):
        """
        plot diagnostic trace plots
        """
        nPigs = np.shape(self.gibbsobj.alpha0gibbs)[1]
        plotNames = self.basicobj.uPigs.astype('str')
        plotArray = self.gibbsobj.alpha0gibbs
        nTotalFigs = nPigs
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        cc = 0
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3, (j+1))
                if cc < nTotalFigs:
                    bn_i = plotArray[:,cc]
                    P.plot(bn_i)
                    P.title('Alpha0 ' + plotNames[cc])
                cc += 1
            P.show(block=lastFigure)


    def traceAlphaNPlot(self):
        """
        plot diagnostic trace plots
        """
        nPigs = np.shape(self.gibbsobj.alphaNgibbs)[1]
        plotNames = self.basicobj.uPigs.astype('str')
        plotArray = self.gibbsobj.alphaNgibbs
        nTotalFigs = nPigs
        nfigures = np.int(np.ceil(nTotalFigs / 6.0))
        cc = 0
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            for j in range(6):
                P.subplot(2,3, (j+1))
                if cc < nTotalFigs:
                    bn_i = plotArray[:,cc]
                    P.plot(bn_i)
                    P.title('AlphaN ' + plotNames[cc])
                cc += 1
            P.show(block=lastFigure)

    def plotWRPCauchy(self):

        datesArr = []
        julRad = []
        d_i = datetime.date(2008, 1, 1)
        datesArr.append(d_i)
        j_i = d_i.timetuple().tm_yday / 366. * 2.0 * np.pi
        julRad.append(j_i)
        for i in range(1, 366):
            d_i = d_i + datetime.timedelta(days = 1)
            datesArr.append(d_i)
            j_i = d_i.timetuple().tm_yday / 366. * 2.0 * np.pi
            julRad.append(j_i)
        nJul = len(julRad)
        julRad = np.array(julRad)
        datesArr = np.array(datesArr)
        wCoast = np.zeros((self.params.ngibbs, nJul))
        for i in range(self.params.ngibbs):
            ## WRAPPED CAUCHY DENSITY FOR DAYS OF OBSERVED LOCATIONS
            dwrpC_i = dwrpcauchy(julRad, self.gibbsobj.agibbs[i], 
                self.gibbsobj.bgibbs[i])
            wCoast[i] = self.gibbsobj.mugibbs[i] + self.gibbsobj.nugibbs[i] * dwrpC_i
        summaryArr = np.zeros((3, nJul))
        summaryArr[0] = np.mean(wCoast, axis = 0)      
        summaryArr[1] = np.array(mquantiles(wCoast, axis=0, prob = 0.025))      
        summaryArr[2] = mquantiles(wCoast, axis=0, prob = 0.975)
        P.figure(figsize = (11,9))
        ax = P.gca()
        lns1 = ax.plot(datesArr, summaryArr[0], color = 'k', linewidth = 3)
        lns2 = ax.plot(datesArr, summaryArr[1], color = 'k', ls = 'dashed')
        lns3 = ax.plot(datesArr, summaryArr[2], color = 'k', ls = 'dashed')
#        for tick in ax.xaxis.get_major_ticks():
#            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)

        monthLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 
            'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        dateLabels = []
        d_i = datetime.date(2008,1,1)
        dateLabels.append(d_i)
        for i in range(11):
            d_i = add_months(d_i, 1)
            dateLabels.append(d_i)

#        yLabel = [-0.8, -0.6, -0.4, -0.2]
        ax.set_xticks(dateLabels)
#        ax.set_yticks(yLabel)
        ax.set_xlabel('Day of year', fontsize = 17)
        ax.set_ylabel(r'$\beta_n$ (population level)', fontsize = 17)
        ax.set_xticklabels(monthLabels, fontsize = 14)
#        ax.set_yticklabels(yLabel, fontsize = 14)
        ax.axhline(y=0, ls = '--', color = 'k')
#        ax.set_ylabel('$\omega$ DCoast', rotation='horizontal', fontsize = 17)
        P.savefig(self.params.coastPlotFname, format='png', dpi = 1200)
        P.show()

    def makeAlphaTable(self):
        structured = np.empty((self.basicobj.nPigs,), dtype=[('Collar ID', 'U12'), 
            ('Mean A0', np.float), ('A0 Low CI', np.float), ('A0 High CI', np.float), 
            ('Mean AN', np.float), ('AN Low CI', np.float), ('AN High CI', np.float)])
        meanA0 = np.round(np.mean(self.gibbsobj.alpha0gibbs, axis = 0), 3)
        meanAN = np.round(np.mean(self.gibbsobj.alphaNgibbs, axis = 0), 3)
        A0LoCI = mquantiles(self.gibbsobj.alpha0gibbs, prob = 0.025, axis = 0)
        A0HiCI = mquantiles(self.gibbsobj.alpha0gibbs, prob = 0.975, axis = 0)
        ANLoCI = mquantiles(self.gibbsobj.alphaNgibbs, prob = 0.025, axis = 0)
        ANHiCI = mquantiles(self.gibbsobj.alphaNgibbs, prob = 0.975, axis = 0)
        structured['Collar ID'] = self.basicobj.uPigs.astype(str)
        structured['Mean A0'] = meanA0
        structured['Mean AN'] = meanAN
        structured['A0 Low CI'] = np.round(A0LoCI, 3)
        structured['A0 High CI'] = np.round(A0HiCI, 3)
        structured['AN Low CI'] = np.round(ANLoCI, 3)
        structured['AN High CI'] = np.round(ANHiCI, 3)
        np.savetxt(self.params.alphaTableFname, structured, 
            fmt=['%s', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f', '%.4f'],
                comments = '', delimiter = ',', 
                header='Names, A0Mean, A0Low_CI, A0High_CI, ANMean, ANLow_CI, ANHigh_CI')


    def plotIndividCoast(self):
        datesArr = []
        monthArr = []
        julRad = []
        d_i = datetime.date(2008, 1, 1)
        datesArr.append(d_i)
        m_i = calendar.month_name[d_i.month]
        monthArr.append(m_i)
        j_i = d_i.timetuple().tm_yday / 366. * 2.0 * np.pi
        julRad.append(j_i)
        for i in range(1, 366):
            d_i = d_i + datetime.timedelta(days = 1)
            datesArr.append(d_i)
            m_i = calendar.month_name[d_i.month]
            monthArr.append(m_i)
            j_i = d_i.timetuple().tm_yday / 366. * 2.0 * np.pi
            julRad.append(j_i)
        nJul = len(julRad)
        julRad = np.array(julRad)
        datesArr = np.array(datesArr)
        monthArr = np.array(monthArr)
        print('len' , len(julRad), len(datesArr))
        nPigs = self.basicobj.nPigs
        collars = self.basicobj.uPigs.astype(str)
        
        monthLabels = ['Jan', 'Jul', 'Dec']
        dateLabels = []
        dateLabels.append(np.min(datesArr))
        dateLabels.append(datetime.date(2008,7,1))
        dateLabels.append(np.max(datesArr))

        fig, axlist = P.subplots(5,3, sharex=True, sharey=True, figsize=(9,16))
        fig.add_subplot(111, frameon = False)
        figCols = np.tile(np.arange(3),5)
        figRows = np.repeat(np.arange(5), 3)
        for i in range(nPigs):
#            P.subplot(5,3, (i+1), sharex = True, sharey=True)
            wCoast = np.zeros((self.params.ngibbs, nJul))
            for j in range(self.params.ngibbs):
                ## WRAPPED CAUCHY DENSITY FOR DAYS OF OBSERVED LOCATIONS
                dwrpC_i = dwrpcauchy(julRad, self.gibbsobj.agibbs[j], 
                    self.gibbsobj.bgibbs[j])
                wCoast[j] = (self.gibbsobj.alpha0gibbs[j, i] + 
                    (self.gibbsobj.alphaNgibbs[j, i] * dwrpC_i))

            summaryArr = np.zeros((3, nJul))
            summaryArr[0] = np.mean(wCoast, axis = 0)      
            summaryArr[1] = np.array(mquantiles(wCoast, axis=0, prob = 0.025))      
            summaryArr[2] = mquantiles(wCoast, axis=0, prob = 0.975)
            col_i = figCols[i]
            row_i = figRows[i]
            ax = axlist[row_i, col_i]


            ax.plot(datesArr, summaryArr[0], color = 'k', linewidth = 3)
            ax.plot(datesArr, summaryArr[1], color = 'k', ls = 'dashed',
                linewidth = 0.75)
            ax.plot(datesArr, summaryArr[2], color = 'k', ls = 'dashed',
                linewidth = 0.75)
            ax.axhline(y=0, ls = '--', color = 'k')

            ax.set_xticks(dateLabels)
            ax.set_xticklabels(monthLabels)
            ax.text(x = datetime.date(2008, 10, 25), y = 1.35, s = collars[i],
                fontsize = 9)
        P.tick_params(labelcolor='none', top=False, bottom=False, left=False, 
                right=False)

        P.xlabel('Days of year', fontsize = 17)
        P.ylabel(r'$\beta_{n,i}$', fontsize = 17)

        P.savefig(self.params.betaNFname, format='png', dpi = 1200)
        P.show()


    def calcDIC(self):
        self.dBar = np.mean(self.gibbsobj.Dgibbs)
        beta_s = np.mean(self.gibbsobj.betagibbs, axis = 0)
        alpha0_s = np.mean(self.gibbsobj.alpha0gibbs, axis = 0)
        alphaN_s = np.mean(self.gibbsobj.alphaNgibbs, axis = 0)
        a_s = np.mean(self.gibbsobj.agibbs)
        b_s = np.mean(self.gibbsobj.bgibbs)
        obsDot_s = np.dot(self.basicobj.xObs, beta_s)
        ## LONG ARRAY OF INDIVIDUAL INTERCEPTS FOR OBS DATA
        obsAlpha0_s = alpha0_s[self.basicobj.obsPigIndx]
        ## LONG ARRAY OF COAST BETAN INDIVIDUAL 
        obsAlphaN_s = alphaN_s[self.basicobj.obsPigIndx]
        dwrpObs_s = dwrpcauchy(self.basicobj.julRadian, a_s, b_s)
        ## BETAN DIRECT PREDICTION
        obsBetaN_s = obsAlpha0_s + (obsAlphaN_s * dwrpObs_s)
        ## REL PROBABILITY OF PRESENCE FOR ALL OBS DATA
        w_s = obsDot_s + (obsBetaN_s * self.basicobj.obsLnDCoast)
        ## RANDOM COVARIATE MATRIX MULTIPLICATION FOR ALL RANDOM DATA
        randDot_s = np.dot(self.basicobj.xRand, beta_s) 
        logLikZ_s = 0.0
        logLikPigZ_s = np.zeros(self.basicobj.nPigs)
        ## RUN LIKELIHOOD FUNCTION IN basicobj
        (logLikZ_s, logLikPigZ_s) = self.basicobj.zLikelihood(alpha0_s, 
            alphaN_s, a_s, b_s, w_s, randDot_s, logLikZ_s, logLikPigZ_s)
        ## CALCULATE DIC
        self.D_MeanTheta = -2.0 * logLikZ_s
        self.DIC = (2.0 * self.dBar) - self.D_MeanTheta  
        self.PD = self.dBar - self.D_MeanTheta

        ## CALCULATE DIC FOR NULL MODEL
        self.basicobj.obsNDat
        self.basicobj.randNDat
        
        dBarNull = -2.0 * np.log(1.0 / 1001.0) * self.basicobj.obsNDat
        meanThetaNull = -2.0 * np.log(1.0 / 1001.0) * self.basicobj.obsNDat
        nullDIC = (2.0 * dBarNull) - meanThetaNull        
        sampleSizeDIC = dBarNull

        print('########################')
        print('##')
        print('##   DIC: ', self.DIC)
        print('##   PD: ', self.PD)
        print('##   nullDIC: ', nullDIC)
        print('##   sampleSizeDIC: ', sampleSizeDIC)
        print('##')
        print('########################')
