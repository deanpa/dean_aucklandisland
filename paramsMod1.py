#!/usr/bin/env python

########################################
########################################
# This file is part of TBfree funded project to quantify g0 and
# sigma parameters for possums across diverse habitats
# Copyright (C) 2016 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
#import initiateModel
#import mcmcRun
import numpy as np


class ModelParams(object):
    """
    Contains the parameters for the mcmc run. This object is also required 
    for the pre-processing. 
    """
    def __init__(self):
        ############################################################
        ###################################### USER MODIFY HERE ONLY
        #################################
        self.modelID = 1
        ###################################################
        ## Set number of MCMC iterations, thin rate and burn in
        self.ngibbs = 3000   #3000  # number of estimates to save for each parameter
        self.thinrate = 20   #30   # 200      # thin rate
        self.burnin = 8000          # burn in number of iterations

        ## If have not initiated parameters, set to 'True'   
        self.firstRun = True   ## True or False         
        print('First Run:', self.firstRun)

        ## array of iteration id to keep
        self.keepseq = np.arange(self.burnin, ((self.ngibbs * self.thinrate) + self.burnin),
            self.thinrate)

        # set paths to scripts and data
        self.inputDataPath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', default = '.'),
            'AuckIsl')
        self.outputDataPath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', default = '.'), 
            'AuckIsl', 'Results', 'mod1Results')


        ## set Data names
        self.basicdataFname = os.path.join(self.outputDataPath, 'basicdataMod1.pkl')
        self.mcmcFname = os.path.join(self.outputDataPath, 'mcmcMod1.pkl')
        self.paramsResFname = os.path.join(self.outputDataPath, 'parameterTableMod1.csv')
        self.individTableFname = os.path.join(self.outputDataPath, 'individTableMod1.csv')
        self.coastPlotFname = os.path.join(self.outputDataPath, 'coastOmega.png')
        self.alphaTableFname = os.path.join(self.outputDataPath, 'alphaTable.csv')
        self.betaNFname = os.path.join(self.outputDataPath, 'betaNIndivid.png')

        ## Input data
        self.inputRandFname = os.path.join(self.inputDataPath, 'pigs_rand1000.csv')
        self.inputObsFname = os.path.join(self.inputDataPath, 'pigs_observed.csv')

        print('params basic path', self.basicdataFname) 
        print('ngibbs', self.ngibbs, 'thin', self.thinrate, 'burnin', self.burnin)

        ## Could add in a data dictionary for defining covariates - later
        
        ######################################
        ######################################
        # modify  variables used for habitat model
        self.xdatDictionary = {'scalePTus200' : 0, 'scaleMnDevN200' : 1, 
                            'scaleD2Riv200' : 2, 'scalePFor200' : 3,
                            'scalePBog200' : 4, 'scalePTus500' : 5, 
                            'scaleMnDevN500' : 6, 'scaleD2Riv500' : 7, 
                            'scalePFor500' : 8, 'scalePBog500' : 9}
        self.scalePTus200 = self.xdatDictionary['scalePTus200']
        self.scaleMnDevN200 = self.xdatDictionary['scaleMnDevN200']
        self.scaleD2Riv200 = self.xdatDictionary['scaleD2Riv200']
        self.scalePFor200 = self.xdatDictionary['scalePFor200']
        self.scalePBog200 = self.xdatDictionary['scalePBog200']

        self.scalePTus500 = self.xdatDictionary['scalePTus500']
        self.scaleMnDevN500 = self.xdatDictionary['scaleMnDevN500']
        self.scaleD2Riv500 = self.xdatDictionary['scaleD2Riv500']
        self.scalePFor500 = self.xdatDictionary['scalePFor500']
        self.scalePBog500 = self.xdatDictionary['scalePBog500']

        # array of index values to get variables from 2-d array
        self.xdatIndx = np.array([self.scalePTus200, self.scaleMnDevN200, 
            self.scaleD2Riv200, self.scalePFor200], dtype = int)
        ######################################
        ######################################

        ## SET INITIAL PARAMETER VALUES
        ### beta parameters: NO INTERCEPT, <<<PTUS, DEVN, DRIV, PFOR>>>, NO DCOAST  
        self.beta = np.array([0.1, -0.1, -0.1, 0.02])
        self.nBeta = len(self.beta)
        self.priorBeta = [0, 10.0]
        self.searchBeta = 0.03
        # WRAPPED CAUCHY PARAMETERS
        self.a = np.pi
        self.b = 1.0
        self.prior_a = [np.pi, 20.0]    ## NORMAL PRIOR WITH HIGH VARIANCE
        self.prior_b = [np.log(1.0), 0.1]   ## LOG NORMAL PRIOR
        self.searchAB = [0.1, 0.1]
        ## INDIVIDUAL MEANS OF ALPHA_0 INTERCEPT, WHICH INFLUENCES DCOAST
        self.Alpha0 = np.random.uniform(-0.2, 0.01, 15)
#        self.Beta0Prior = [0.0, 10.0]
        self.searchAlpha0 = 0.2
        ## POPULATION VARIANCE PARAMETER OF Alpha INTERCEPT INDIVIDUAL EFFECT
        self.sigma = 0.1 # VARIANCE NOT SD !!!!!!
        self.sigmaPrior = [.1, .1]  ## INVERSE GAMMA PRIOR
        self.searchSigma = 0.01
        ## POPULATION MEAN OF ALPHA_0 INTERCEPT
        self.mu = 0.1
        self.muPrior = [0.0, 100]
#        self.ppart = np.dot(self.vinvert, np.zeros(self.nPigs))
        self.s1 = .1
        self.s2 = .1

#        self.searchMu = 0.02
        ## INDIVIDUAL MEANS OF Alpha_DCOAST
        self.AlphaN = np.random.uniform(-0.1, 0.1, 15)
        self.AlphaNPrior = [0.0, 10.0]
        self.searchAlphaN = 0.2
        ## POPULATION VARIANCE PARAMETER OF Alpha DCOAST INDIVIDUAL EFFECT
        self.epsil = 0.1
        self.priorEpsil = [0.1, 0.1]    ## INVERSE GAMMA PRIOR
#        self.searchEpsil = 0.01
        ## POPULATION MEAN OF Alpha DCOAST
        self.nu = 0.1
        self.priorNu = [0.0, 10.0]
#        self.searchNu = 0.02

        






