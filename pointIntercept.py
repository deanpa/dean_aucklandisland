#!/usr/bin/env python

import os
#from scipy import stats
#from scipy.special import gammaln
#from scipy.special import gamma
import numpy as np
from numba import jit
#import pickle
#import datetime
from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo import gdalconst
import pandas as pd


COMPRESSED_HFA = ['COMPRESSED=YES']


@jit
def loopCovarRaster(ndat, x, y, rast, ulx, uly, cov_i, xres, yres):
    """
    ## LOOP THRU RASTER I AND GET DATA FOR ALL POINTS
    """ 
    for j in range(ndat):
        col = np.int((x[j] - ulx) / xres)
        ## HAVE TO MAKE yres POSITIVE; NORMALLY IT IS NEGATIVE
        row = np.int((uly - y[j]) / -yres)
        cov_i[j] = rast[row, col]
    return(cov_i)



class InterceptData(object):
    def __init__(self, ptInputFname, rastFname, ptOutputFname, xName, yName):
        """
        Object to extract data from raster 
        """
        self.ptInputFname = ptInputFname
        self.rastFname = rastFname
        self.ptOutputFname= ptOutputFname
        self.xName = xName
        self.yName = yName

        ################ RUN FUNCTIONS
        self.readPtData()
        self.getRasterData()
        self.writePandasFile()

    def readPtData(self):
        """
        ## READ IN DATA FROM CSV
        """
        self.locDat = pd.read_csv(self.ptInputFname, delimiter=',')
        self.x = np.array(self.locDat[[self.xName]]).flatten()
        self.y = np.array(self.locDat[[self.yName]]).flatten()
        self.nDat = len(self.x)

    def getRasterData(self):
        """
        ## GET DATA FROM RASTERS 
        """
        ## OPEN RASTER AS ARRAY
        src = gdal.Open(self.rastFname)
        (ulx, xres, xskew, uly, yskew, yres) = src.GetGeoTransform()
        rastData = src.ReadAsArray()
        print(np.shape(rastData), 'res', xres, yres)
        ## EMPTY ARRAY TO POPULATE
        self.cov_i = np.zeros(self.nDat)
        ## LOOP THRU DATA TO GET RASTER VALUES
        cov_i = loopCovarRaster(self.nDat, self.x, self.y, rastData, ulx, uly, 
                self.cov_i, xres, yres)
      


    def writePandasFile(self):
        """
        write pandas df to directory
        """
        base = os.path.basename(self.rastFname)
        fName = os.path.splitext(base)[0]
        print('fname', fName)
        self.locDat.loc[:, fName] = self.cov_i
        self.locDat.to_csv(self.ptOutputFname, sep=',', header=True, index_label = 'did')










######################
# Main function
def main():
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', default = '.'), 
        'AuckIsl')
#    outputDataPath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', default = '.'), 
#            'AuckIsl', 'Covariates')

    ## SET X AND Y COORDINATE NAMES AS IN POINT DATA
    xName = 'xcoord'
    yName = 'ycoord'

    ## set Data names
    ptInputFname = os.path.join(inputDataPath, 'pigs_1000RP_XY2.csv')    #pigs_cullHighVelocity_2.csv
    rastFname = os.path.join(inputDataPath, 'Covariates', 'dCoast.img')
    ptOutputFname = os.path.join(inputDataPath, 'pigs_rand1000.csv')    #'pigs_100MCP_clip.

    interceptdata = InterceptData(ptInputFname, rastFname, ptOutputFname,
        xName, yName)

if __name__ == '__main__':
    main()


