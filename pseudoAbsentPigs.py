#!/usr/bin/env python

import os
#from scipy import stats
#from scipy.special import gammaln
#from scipy.special import gamma
import numpy as np
from numba import jit
#import pickle
#import datetime
from osgeo import gdal
from osgeo import ogr
from osgeo import osr
from osgeo import gdalconst

COMPRESSED_HFA = ['COMPRESSED=YES']

@jit
def loopRaster(rast_i, x_i, y_i, randCells, xmin, ymax, nrows, ncols, 
        totalPseudo, resol):
    """
    ## LOOP RASTER AND GET X Y LOCS FOR PSEUDO POINTS
    """
    xminPt = xmin + (0.5 * resol)
    ymaxPt = ymax - (0.5 * resol)
    for j in range(totalPseudo):
        cc = 0
        for rowCount in range(nrows):
            for colCount in range(ncols):
                if rast_i[rowCount, colCount] == 0:
                    continue
                cc += 1
                if ((cc - 1) == randCells[j]):
#                    dx = np.random.uniform(-0.2*resol, 0.2*resol)
#                    dy = np.random.uniform(-0.2*resol, 0.2*resol)
                    x_i[j] = xminPt + (colCount * resol)  #+ dx
                    y_i[j] = ymaxPt - (rowCount * resol)  #+ dy
                    break
            if ((cc - 1) == randCells[j]):
                break


@jit
def loopCovarRaster(ndat, x, y, rast, ulx, uly, cov_i, xres, yres):
    """
    ## LOOP THRU RASTER I AND GET DATA FOR ALL POINTS
    """ 
    for j in range(ndat):
        col = np.int((x[j] - ulx) / xres)
        ## HAVE TO MAKE yres POSITIVE; NORMALLY IT IS NEGATIVE
        row = np.int((uly - y[j]) / -yres)
        cov_i[j] = rast[row, col]
    return(cov_i)



class SpatialData(object):
    def __init__(self, inputDataPath, outputDataPath, shapeFname, 
        locationFname, pseudoAbsentFname, resol, tmpRasterFname, nRandLoc):
        """
        Object to get random location 
        """
        self.resol = resol
        self.nRandLoc = nRandLoc
        self.tmpRasterFname = tmpRasterFname
        self.pseudoAbsentFname = pseudoAbsentFname
        #############################################
        ############ Run spatial functions

        self.readLocationData(locationFname)
        self.wrapGetLocFX(shapeFname)
        self.writeToFile()
        #############################################

    def readLocationData(self, locationFname):
        """
        READ IN ARGOS LOCATION AND COVARIATE DATA
        """
        locDataTypes = np.array(['i8', 'f8', 'f8', 'i8', 'S32', 'i8', 'i8', 'f8', 'i8', 'i8'])
        locDataTypes = np.append(locDataTypes, np.repeat('f8', 18))
        self.locDat = np.genfromtxt(locationFname,  delimiter=',', names=True, dtype=locDataTypes)
        # specify arrays
        self.pigID = self.locDat['collar']
        self.uPigs = np.unique(self.pigID)
        self.nPigs = len(self.uPigs)
        self.timeDate = self.locDat['timeDate']
#        self.x = self.locDate['x']
        print('u pigs', self.uPigs, 'n pigs', self.nPigs)
        

    def wrapGetLocFX(self, shapeFname):
        """
        ## WRAPPRER FX; LOOP PIGS AND GET PSEUDO LOCS
        """
        self.collar = []
        self.xRand = []
        self.yRand = []
        self.dtRand = []
        ## OPEN MCP SHAPEFILE
        driver = ogr.GetDriverByName("ESRI Shapefile")
        shapeData = driver.Open(shapeFname)
        ## LOOP THROUGH PIGS
        for i in range(self.nPigs):
            ## TEMP RASTER FOR PIG I
            pigRast_i  = self.getPigRaster(i, shapeData)
            self.getPigInfo(i)
        ## CONVERT TO 1-D ARRAYS
        self.collar = np.concatenate(self.collar)
        self.xRand = np.concatenate(self.xRand)
        self.yRand = np.concatenate(self.yRand)
        self.dtRand = np.concatenate(self.dtRand)


    def getPigRaster(self, i, shapeData):
        pid = self.uPigs[i]
        pigLayer = shapeData.GetLayer()
        spatialRef = pigLayer.GetSpatialRef()
        auck_WKT = spatialRef.ExportToWkt()
#        print('spatialRef', spatialRef, 'WKT Auckland Island', auck_WKT)
        ## GET SPATIAL INFO ON MCP I
        pigLayer.SetAttributeFilter("collar = %i" % pid)        
        (self.xmin, self.xmax, self.ymin, self.ymax) = pigLayer.GetExtent()
        self.cols = int((self.xmax - self.xmin) / self.resol)
        self.rows = int((self.ymax - self.ymin) / self.resol)
        self.match_geotrans = [self.xmin, self.resol, 0, self.ymax, 0, -self.resol]
        ## CREATE DESTINATION TEMP IMG
        driver = gdal.GetDriverByName('HFA')
        pig_ds = driver.Create(self.tmpRasterFname, self.cols, 
            self.rows, 1, gdal.GDT_Byte, COMPRESSED_HFA)
        pig_ds.SetGeoTransform(self.match_geotrans)
        pig_ds.SetProjection(auck_WKT)
        band = pig_ds.GetRasterBand(1)
        NoData_value = 0
        band.SetNoDataValue(NoData_value)
        # Rasterize mcp and write to directory
        gdal.RasterizeLayer(pig_ds, [1], pigLayer, burn_values=[1])
        pig_ds.FlushCache()
        del pigLayer
        del pig_ds

    def getPigInfo(self, i):
        """
        ## GET INFO ON PIG I
        """
        mask_i = self.pigID == self.uPigs[i]
        nLoc_i = np.sum(mask_i)
        dt_i = self.timeDate[mask_i]
        ## OPEN THE RASTER OF THE MCP FOR PIG I
        rast_i = gdal.Open(self.tmpRasterFname).ReadAsArray()
        nCells = np.sum(rast_i)
        totalPseudo = nLoc_i * self.nRandLoc
        collar_i = np.repeat(self.uPigs[i], totalPseudo)
        time_i = np.repeat(dt_i, self.nRandLoc)
        x_i = np.zeros(totalPseudo)
        y_i = np.zeros(totalPseudo)
        ## SELECT RANDOM CELLS TO PUT THE PSEUDO ABSENT POINTS
        randCells = np.random.choice(np.arange(nCells), totalPseudo) 
        ## LOOP THRU ROWS AND COLS OF RASTER ARRAY
        loopRaster(rast_i, x_i, y_i, randCells, self.xmin, self.ymax, 
            self.rows, self.cols, totalPseudo, self.resol)
        ## APPEND ARRAYS TO LISTS
        self.collar.append(collar_i)
        self.xRand.append(x_i)
        self.yRand.append(y_i)
        self.dtRand.append(time_i)




    def writeToFile(self):
        nDat = len(self.xRand)
        structured = np.empty((nDat,), dtype=[('collar', 'i8'), ('x', np.float),
            ('y', np.float), ('time', 'U32')])
        ## COPY DATA OVER
        structured['collar'] = self.collar
        structured['x'] = self.xRand
        structured['y'] = self.yRand
        structured['time'] = self.dtRand
        np.savetxt(self.pseudoAbsentFname, structured, fmt=['%i', '%.1f', '%.1f', '%19s'],
            delimiter = ',', comments='',
            header='collar, x, y, dateTime')


class CovariateData(object):
    def __init__(self, inputDataPath, covariateDir, pseudoAbsentFname, 
        pseudoCovarFname):
        """
        Object to get random location 
        """
        ## MOVE FILE NAMES AND DIRECTORIES TO SELF
        self.pseudoAbsentFname = pseudoAbsentFname
        self.covariateDir = covariateDir
        self.pseudoCovarFname = pseudoCovarFname
        #########################
        ## RUN FUNCTIONS
        self.readPseudoData()
        self.getFileList()
        self.getCovariates()


    def readPseudoData(self):
        datTypes = ['i8', 'f8', 'f8', 'S32']
        self.pseudoDat = np.genfromtxt(self.pseudoAbsentFname,  delimiter=',', 
            names=True, dtype=datTypes)
        # specify arrays
        self.Collar = self.pseudoDat['collar']
        self.X = self.pseudoDat['x']
        self.Y = self.pseudoDat['y']
        self.DateTime = self.pseudoDat['dateTime']
#        print('datetime', self.DateTime[:10])

    def getFileList(self):
        self.rasterFileList = []
        self.rasterNameList = []
        for root, dirs, files in os.walk(self.covariateDir):
            for file in files:
                if file.endswith('.img'):
                    self.rasterFileList.append(file)
                    fName = os.path.splitext(file)[0]
                    self.rasterNameList.append(fName)
#                    print('file', file, 'fName', fName)
#        print('rasterList', self.rasterFileList, 'rasterName', self.rasterNameList)

    def getCovariates(self):
        ndat = len(self.X)
        nCov = len(self.rasterNameList)
        cov_i = np.zeros(ndat)
        ## MAKE STRUCTURED ARRAY FOR WRITING TO DIRECTORY
        covDataType = [('collar', 'i8'), ('x', np.float), ('y', np.float), 
            ('time', 'U32')]
        colHeader = ['collar, x, y, dateTime']
        colFmt = ['%i', '%.1f', '%.1f', '%19s']
        for i in range(nCov):
            addCov = (self.rasterNameList[i], 'i8')
            covDataType.append(addCov)
            colHeader.append(self.rasterNameList[i])
            colFmt.append('%f')
        structured = np.empty((ndat,), dtype = covDataType)
        structured['collar'] = self.Collar
        structured['x'] = self.X
        structured['y'] = self.Y
        structured['time'] = self.DateTime
        
        ## GET DATA FROM RASTERS 
        for i in range(nCov):
            rastName = self.rasterNameList[i]
            rastFile = os.path.join(self.covariateDir, self.rasterFileList[i])
            ## OPEN RASTER AS ARRAY
            src = gdal.Open(rastFile)
            (ulx, xres, xskew, uly, yskew, yres)  = src.GetGeoTransform()
            rastData = src.ReadAsArray()
#            print('i', i, 'shape', np.shape(rastData), 'res', xres, yres)
            ## LOOP THRU DATA TO GET RASTER VALUES
            cov_i = loopCovarRaster(ndat, self.X, self.Y, rastData, ulx, uly, 
                cov_i, xres, yres)
            structured[rastName] = cov_i
        ## SAVE STRUCTURED ARRAY TO DIR, HEADER MAKES USE OF JOIN FUNCTION ON LIST
        np.savetxt(self.pseudoCovarFname, structured, fmt = colFmt,
            delimiter = ',', comments='', header = ','.join(colHeader))



######################
# Main function
def main():
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', default = '.'))
    outputDataPath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', default = '.'), 
            'Results')
    covariateDir = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', default = '.'), 
            'Covariates')

    ## RESOLUTION
    resol = 2.0
    ## NUMBER OF RAND POINTS PER ARGOS DATUM
    nRandLoc = 50
    ## set Data names
    shapeFname = os.path.join(inputDataPath, 'mcp100_NoIslands.shp')    #'pigs_100MCP_clip.shp')
    locationFname = os.path.join(inputDataPath, 'pigs_cullHighVelocity_2.csv')
    pseudoAbsentFname = os.path.join(inputDataPath, 'pseudoAbsentData.csv')
    tmpRasterFname = os.path.join(inputDataPath, 'tmpRast.img')
    pseudoCovarFname = os.path.join(inputDataPath, 'pseudoCovar.csv')

    spatialdata = SpatialData(inputDataPath, outputDataPath, shapeFname, 
        locationFname, pseudoAbsentFname, resol, tmpRasterFname, nRandLoc)

    covariatedata = CovariateData(inputDataPath, covariateDir, pseudoAbsentFname,
        pseudoCovarFname)

if __name__ == '__main__':
    main()


