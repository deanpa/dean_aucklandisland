#!/usr/bin/env python

########################################
########################################
# This file is part of Possum g0 and sigma analysis
# Copyright (C) 2016 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

import os
import pickle
import numpy as np
from scipy import stats
import pandas as pd
import datetime
from numba import jit


def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)


### Define global functions: ###
def logit(x):
    return np.log(x) - np.log(1 - x)


def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))



def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D

def dwrpcauchy(th, mu, rho):
    """
    wrapped cauchy pdf: direction is mu, and focus is rho.
    mu is real, and rho > 0;  Wikipedia pdf equation
    """
    sinh_rho = np.sinh(rho)
    cosh_rho = np.cosh(rho)
    cos_mu_th = np.cos(th - mu)
    dwrpc = sinh_rho / 2 / np.pi / (cosh_rho - cos_mu_th)
    return dwrpc

 

class BasicData(object):
    def __init__(self, params):
        """
        Object to read in  data
        Import updatable params from params
        set initial values
        """
#        np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
        #############################################
        ############ Run basicdata functions
        self.getParams(params)
        self.readData()
        self.getDateTime()
        self.getPigIndx()
        self.makeCovariateData()
        (self.logLikZ, self.logLikPigZ) = self.zLikelihood(self.Alpha0, 
            self.AlphaN, self.a, self.b, 
            self.w, self.randDot, self.logLikZ, self.logLikPigZ)

        ############## End run of basicdata functions
        #############################################

    def getParams(self, params):
        """
        # move params parameters into basicdata
        """
        self.params = params
        self.beta = self.params.beta
        self.Alpha0 = self.params.Alpha0
        self.AlphaN = self.params.AlphaN
        self.a = self.params.a
        self.b = self.params.b        
        self.nu = self.params.nu          
        self.epsil = self.params.epsil
        self.mu = self.params.mu
        self.sigma = self.params.sigma
        
    def readData(self):
        """
        ## READ IN DATA FROM CSV
        """
        self.obsDat = pd.read_csv(self.params.inputObsFname, delimiter=',')
        self.randDat = pd.read_csv(self.params.inputRandFname, delimiter=',')
        print('Obs shp', self.obsDat.shape, 'Rand shp', self.randDat.shape)


        self.obsCollar = np.array(self.obsDat[['collar']]).flatten()
        self.obsTimeDate = np.array(self.obsDat[['timeDate']]).flatten()
        self.obsPTuss200 = np.array(self.obsDat[['ptus200']]).flatten()
        self.obsMnDevN200 = np.array(self.obsDat[['mdevN200']]).flatten()
        self.obsD2Riv200 = np.array(self.obsDat[['md2riv200']]).flatten()
        self.obsPFor200 = np.array(self.obsDat[['pfor200']]).flatten()
        self.obsDCoast = np.array(self.obsDat[['dCoast']]).flatten()
        self.obsPBog200 = np.array(self.obsDat[['pbog200']]).flatten()
        self.obsNDat = len(self.obsCollar)

        self.randCollar = np.array(self.randDat[['Collar']]).flatten()
        self.randPTuss200 = np.array(self.randDat[['ptus200']]).flatten()
        self.randMnDevN200 = np.array(self.randDat[['mdevN200']]).flatten()
        self.randD2Riv200 = np.array(self.randDat[['md2riv200']]).flatten()
        self.randPFor200 = np.array(self.randDat[['pfor200']]).flatten()
        self.randDCoast = np.array(self.randDat[['dCoast']]).flatten()
        self.randPBog200 = np.array(self.randDat[['pbog200']]).flatten()
        self.randNDat = len(self.randCollar)


        self.obsPTuss500 = np.array(self.obsDat[['ptus500']]).flatten()
        self.obsMnDevN500 = np.array(self.obsDat[['mdevN500']]).flatten()
        self.obsD2Riv500 = np.array(self.obsDat[['md2riv500']]).flatten()
        self.obsPFor500 = np.array(self.obsDat[['pfor500']]).flatten()
        self.obsPBog500 = np.array(self.obsDat[['pbog500']]).flatten()

        self.randPTuss500 = np.array(self.randDat[['ptus500']]).flatten()
        self.randMnDevN500 = np.array(self.randDat[['mdevN500']]).flatten()
        self.randD2Riv500 = np.array(self.randDat[['md2riv500']]).flatten()
        self.randPFor500 = np.array(self.randDat[['pfor500']]).flatten()
        self.randPBog500 = np.array(self.randDat[['pbog500']]).flatten()



    def getDateTime(self):
        self.dateTimeFull = []
        julYear = []
        for i in range(self.obsNDat):
            date_i = datetime.datetime.strptime(self.obsTimeDate[i], 
                '%Y-%m-%d %H:%M:%S')
            julYear_i = date_i.timetuple().tm_yday
            self.dateTimeFull.append(date_i)
            julYear.append(julYear_i)
        self.dateTimeFull = np.array(self.dateTimeFull)
        julYear = np.array(julYear)
        maxJul = datetime.date(2007,12,31).timetuple().tm_yday
#        print('maxJul', maxJul)
        self.julRadian = julYear/maxJul * np.pi * 2.0

    def getPigIndx(self):
        self.uPigs = np.unique(self.obsCollar)
        self.nPigs = len(self.uPigs)
        self.pigIndx = np.arange(self.nPigs, dtype = int)
        self.nObsByPig = np.zeros(self.nPigs, dtype = int)
        self.obsPigIndx = np.zeros(self.obsNDat, dtype = int)
        self.randPigIndx = np.zeros(self.randNDat, dtype = int)
        for i in range(self.nPigs):
            pig_i = self.uPigs[i]
            obsMask = self.obsCollar == pig_i
            randMask = self.randCollar == pig_i
            self.obsPigIndx[obsMask] = i
            self.randPigIndx[randMask] = i
            self.nObsByPig[i] = np.sum(obsMask)

            

    def makeCovariateData(self):
        """
        ## CREATE DATA STRUCTURES FOR COVARIATES
        """
        meanPTus200 = np.mean(np.append(self.obsPTuss200, self.randPTuss200))
        sdPTus200 = np.std(np.append(self.obsPTuss200, self.randPTuss200))
        meanMnDevN200 = np.mean(np.append(self.obsMnDevN200, self.randMnDevN200))
        sdMnDevN200 = np.std(np.append(self.obsMnDevN200, self.randMnDevN200))
        meanD2Riv200 = np.mean(np.append(self.obsD2Riv200, self.randD2Riv200))
        sdD2Riv200 = np.std(np.append(self.obsD2Riv200, self.randD2Riv200))
        meanPFor200 = np.mean(np.append(self.obsPFor200, self.randPFor200))
        sdPFor200 = np.std(np.append(self.obsPFor200, self.randPFor200))
        meanPBog200 = np.mean(np.append(self.obsPBog200, self.randPBog200))
        sdPBog200 = np.std(np.append(self.obsPBog200, self.randPBog200))

        meanPTus500 = np.mean(np.append(self.obsPTuss500, self.randPTuss500))
        sdPTus500 = np.std(np.append(self.obsPTuss500, self.randPTuss500))
        meanMnDevN500 = np.mean(np.append(self.obsMnDevN500, self.randMnDevN500))
        sdMnDevN500 = np.std(np.append(self.obsMnDevN500, self.randMnDevN500))
        meanD2Riv500 = np.mean(np.append(self.obsD2Riv500, self.randD2Riv500))
        sdD2Riv500 = np.std(np.append(self.obsD2Riv500, self.randD2Riv500))
        meanPFor500 = np.mean(np.append(self.obsPFor500, self.randPFor500))
        sdPFor500 = np.std(np.append(self.obsPFor500, self.randPFor500))
        meanPBog500 = np.mean(np.append(self.obsPBog500, self.randPBog500))
        sdPBog500 = np.std(np.append(self.obsPBog500, self.randPBog500))

        ## SCALE PARAMETERS
        ## OBS DATA
        xDatObsFull = np.zeros((self.obsNDat, len(self.params.xdatDictionary))) 
        xDatObsFull[:,0] = (self.obsPTuss200 - meanPTus200) / sdPTus200 
        xDatObsFull[:,1] = (self.obsMnDevN200 - meanMnDevN200) / sdMnDevN200 
        xDatObsFull[:,2] = (self.obsD2Riv200 - meanD2Riv200) / sdD2Riv200 
        xDatObsFull[:,3] = (self.obsPFor200 - meanPFor200) / sdPFor200 
        xDatObsFull[:,4] = (self.obsPBog200 - meanPBog200) / sdPBog200 
        xDatObsFull[:,5] = (self.obsPTuss500 - meanPTus500) / sdPTus500 
        xDatObsFull[:,6] = (self.obsMnDevN500 - meanMnDevN500) / sdMnDevN500 
        xDatObsFull[:,7] = (self.obsD2Riv500 - meanD2Riv500) / sdD2Riv500 
        xDatObsFull[:,8] = (self.obsPFor500 - meanPFor500) / sdPFor500 
        xDatObsFull[:,9] = (self.obsPBog500 - meanPBog500) / sdPBog500 

        ## RAND DATA
        xDatRandFull = np.zeros((self.randNDat, len(self.params.xdatDictionary))) 
        xDatRandFull[:,0] = (self.randPTuss200 - meanPTus200) / sdPTus200 
        xDatRandFull[:,1] = (self.randMnDevN200 - meanMnDevN200) / sdMnDevN200 
        xDatRandFull[:,2] = (self.randD2Riv200 - meanD2Riv200) / sdD2Riv200 
        xDatRandFull[:,3] = (self.randPFor200 - meanPFor200) / sdPFor200 
        xDatRandFull[:,4] = (self.randPBog200 - meanPBog200) / sdPBog200
        xDatRandFull[:,5] = (self.randPTuss500 - meanPTus500) / sdPTus500 
        xDatRandFull[:,6] = (self.randMnDevN500 - meanMnDevN500) / sdMnDevN500 
        xDatRandFull[:,7] = (self.randD2Riv500 - meanD2Riv500) / sdD2Riv500 
        xDatRandFull[:,8] = (self.randPFor500 - meanPFor500) / sdPFor500 
        xDatRandFull[:,9] = (self.randPBog500 - meanPBog500) / sdPBog500
 
        ## MAKE REDUCED COVARIATE ARRAY FOR ANALYSIS
        self.xObs = xDatObsFull[:, self.params.xdatIndx]
        self.xRand = xDatRandFull[:, self.params.xdatIndx]

        ## SCALE OF DCOAST
        meanDCoast = np.mean(np.append(self.obsDCoast, self.randDCoast))
        sdDCoast = np.std(np.append(self.obsDCoast, self.randDCoast))
        self.obsLnDCoast = (self.obsDCoast - meanDCoast) / sdDCoast
        self.randLnDCoast = (self.randDCoast - meanDCoast) / sdDCoast
#        ## LN OF DCOAST
#        self.obsLnDCoast = np.log(self.obsDCoast + 1.0)
#        self.randLnDCoast = np.log(self.randDCoast + 1.0)
        self.logLikZ = 0.0
        self.logLikPigZ = np.zeros(self.nPigs)

        ## INITIAL WRAPPED CAUCHY DENSITY FOR DAYS OF OBSERVED LOCATIONS
        self.dwrpObs = dwrpcauchy(self.julRadian, self.a, self.b)
        ## COVARIATE MATRIX MULTIPLICAION
        self.obsDot = np.dot(self.xObs, self.beta)
        ## LONG ARRAY OF INDIVIDUAL INTERCEPTS FOR OBS DATA
        self.obsAlpha0 = self.Alpha0[self.obsPigIndx]
        ## LONG ARRAY OF COAST AlphaN INDIVIDUAL 
        self.obsAlphaN = self.AlphaN[self.obsPigIndx]
        ## BETAN DIRECT PREDICTION
        self.obsBetaN = self.obsAlpha0 + (self.obsAlphaN * self.dwrpObs)
        ## DISTANCE TO COAST TIME WRAPPED CAUCHY FOR OBS DATA
        self.obsCoast = self.obsBetaN * self.obsLnDCoast
        ## REL PROBABILITY OF PRESENCE FOR ALL OBS DATA
        self.w = self.obsDot + self.obsCoast
        ## RANDOM COVARIATE MATRIX MULTIPLICATION FOR ALL RANDOM DATA
        self.randDot = np.dot(self.xRand, self.beta)  




    def zLikelihood(self, Alpha0, AlphaN, a, b, w, randDot,
            logLikZ, logLikPigZ):
#        cc = 0
        ## LOOP PIGS
        for i in range(self.nPigs):
            pig_i = self.uPigs[i]
            randMask = self.randCollar == pig_i
            randDot_i = randDot[randMask]
            randLnDCoast_i = self.randLnDCoast[randMask]
            obsMask = self.obsCollar == pig_i
            julRad_i = self.julRadian[obsMask]
            w_i = w[obsMask]
    #        B0_BetaDot = Beta0[i] + randDot_i
            AlphaN_i = AlphaN[i] 
            Alpha0_i = Alpha0[i]
            ## LOOP THRU ALL OBSERVATIONS FOR PIG I TO GET P(Z | PARAMETERS)
            for j in range(self.nObsByPig[i]):
                randWrpC_j = dwrpcauchy(julRad_i[j], a, b)
                randBetaN_j = Alpha0_i + (AlphaN_i * randWrpC_j)
                ## REL PROBABILITY (OMEGA) OF ALL RAND DATA
                w_rand = np.exp(randDot_i + (randBetaN_j * randLnDCoast_i))
                ## EQ. 1 
                EXP_W = np.exp(w_i[j])
                LLikZ = np.log(EXP_W / (np.sum(w_rand) + EXP_W))
                ## LIKELIHOOD OF ALL DATA GIVEN PARAMETERS  
                logLikZ += LLikZ
                ## LIKELIHOOD OF DATA FOR PIG I GIVEN PARAMETERS
                logLikPigZ[i] += LLikZ
#                cc += 1
#        print('basicdata lik', logLikZ, np.sum(logLikPigZ))   
        return(logLikZ, logLikPigZ)







