#!/usr/bin/env python

import os
#from scipy import stats
#from scipy.special import gammaln
#from scipy.special import gamma
import numpy as np
#from numba import jit
import pickle
#import datetime
import basicsModule
import empiricalPigs


######################
# Main function
def main(params):
    ## if first mcmc run, initiate parameters; else read in basicdata
    if params.firstRun:
        x = 1
        ## create an instance of the basicdata class in initiate model.
        basicdata = basicsModule.BasicData(params)        
    else:
        ## unpickle basicdata results from previous mcmc run.
        fileobj = open(params.basicdataFname, 'rb')
        basicdata = pickle.load(fileobj)
        fileobj.close()
    ## RUN MCMC        
    mcmcobj = empiricalPigs.MCMC(params, basicdata)
    
    ## pickle basic data from present run to be used to initiate new runs
    fileobj = open(params.basicdataFname, 'wb')
    pickle.dump(basicdata, fileobj)
    fileobj.close()

    ## pickle mcmc results for post processing in postProcessing.py
    fileobj = open(params.mcmcFname, 'wb')
    pickle.dump(mcmcobj, fileobj)
    fileobj.close()


if __name__ == '__main__':
    main()


