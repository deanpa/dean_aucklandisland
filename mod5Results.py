#!/usr/bin/env python


from paramsMod5 import ModelParams
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
#import datetime
#from numba import jit
import empiricalResults

def main():
    params = ModelParams()
    print('########################')
    print('########################')
    print('###')
    print('#    Model 5')
    print('###')
    print('########################')
    print('########################')

    # paths and data to read in

    pigpath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', default = '.'),
            'AuckIsl', 'Results', 'mod5Results')


    fileobj = open(params.mcmcFname, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    fileobj = open(params.basicdataFname, 'rb')
    basicobj = pickle.load(fileobj)
    fileobj.close()

    print('gibbsResults Name:', params.mcmcFname)    

    resultsobj = empiricalResults.ResultsProcessing(gibbsobj, basicobj, params, pigpath)

    resultsobj.makeTableFX()

    resultsobj.writeToFileFX()

    resultsobj.tracePlotFX()

    resultsobj.traceAlpha0Plot()

    resultsobj.traceAlphaNPlot()

    resultsobj.makeAlphaTable()

#    resultsobj.plotWRPCauchy()

#    resultsobj.plotIndividCoast()

    resultsobj.calcDIC()

if __name__ == '__main__':
    main()


