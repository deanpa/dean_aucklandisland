#!/usr/bin/env python

########################################
########################################
# This file is part of OSPRI possum g0 and sigma project
# Copyright (C) 2018 Dean Anderson 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# ADDITIONAL NOTES ABOUT THIS SCRIPT.
########################################
########################################

### Import modules: ###
import os
from scipy import stats
from scipy.special import gammaln
from scipy.special import gamma
import numpy as np
from numba import jit
import pickle
from basicsModule import dwrpcauchy

def gamma_pdf(xx, shape, scale):
    gampdf = 1.0 / (scale**shape) / gamma(shape) * xx**(shape - 1) * np.exp(-(xx/scale))
    return gampdf


def logit(x):
    return np.log(x) - np.log(1 - x)

def inv_logit(x):
    return np.exp(x) / (1 + np.exp(x))


def meanGibbsUpdate(y, n, var, prior):
    """
    Gibbs updater for means
    """
    priorVarInv = 1.0 / prior[1]
    nVarInv = n / var
    V = (priorVarInv + nVarInv)**-1
    priorMeanVar = prior[0] / prior[1]
    nyVarInv = n * np.mean(y) / var
    v = priorMeanVar + nyVarInv
    muOut = np.random.normal(v*V, V)
    return(muOut) 
    

def varGibbsUpdate(s1, s2, y, mu, n):
    predDiff = y - mu
    sx = np.sum(predDiff**2.0)
    u1 = (n / 2.0) + s1
    u2 = s2 + (0.5 * sx)
    iVar = np.random.gamma(u1, u2)
    return(1.0 / iVar)



class MCMC(object):
    def __init__(self, params, basicdata):

        self.basicdata = basicdata
        self.params = params

        self.betagibbs = np.zeros((self.params.ngibbs, self.params.nBeta))             # beta parameters
        self.alpha0gibbs = np.zeros((self.params.ngibbs, self.basicdata.nPigs))
        self.alphaNgibbs = np.zeros((self.params.ngibbs, self.basicdata.nPigs))
        self.bgibbs = np.zeros(self.params.ngibbs)             
        self.agibbs = np.zeros(self.params.ngibbs)
        self.mugibbs = np.zeros(self.params.ngibbs)
        self.sigmagibbs = np.zeros(self.params.ngibbs)                                # home range decay
        self.nugibbs = np.zeros(self.params.ngibbs)
        self.epsilgibbs = np.zeros(self.params.ngibbs)                                # home range decay
        self.Dgibbs = np.zeros(self.params.ngibbs)                                # home range decay


        ## run mcmcFX - gibbs loop
        self.mcmcFX()

    def beta_UpdateFX(self):
        beta_s = np.random.normal(self.basicdata.beta, self.params.searchBeta)
        obsDot_s = np.dot(self.basicdata.xObs, beta_s)
        ## REL PROBABILITY OF PRESENCE FOR ALL OBS DATA
        w_s = obsDot_s + self.basicdata.obsCoast
        ## RANDOM COVARIATE MATRIX MULTIPLICATION FOR ALL RANDOM DATA
        randDot_s = np.dot(self.basicdata.xRand, beta_s) 
        logLikZ_s = 0.0
        logLikPigZ_s = np.zeros(self.basicdata.nPigs)
        ## RUN LIKELIHOOD FUNCTION IN BASICDATA
        (logLikZ_s, logLikPigZ_s) = self.basicdata.zLikelihood(self.basicdata.Alpha0, 
            self.basicdata.AlphaN, self.basicdata.a, self.basicdata.b, 
            w_s, randDot_s, logLikZ_s, logLikPigZ_s)
#        print('fx lik', logLikZ_s, np.sum(logLikPigZ_s))
        ## BETA PRIORS
        prior = np.sum(stats.norm.logpdf(self.basicdata.beta, self.params.priorBeta[0],
            self.params.priorBeta[1]))
        prior_s = np.sum(stats.norm.logpdf(beta_s, self.params.priorBeta[0],
            self.params.priorBeta[1]))
        ##CALCULATE IMPORTANCE RATIO
        pnow = np.sum(self.basicdata.logLikZ) + prior
        pnew = np.sum(logLikZ_s) + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
#        print('present', self.basicdata.logLikZ, prior,np.round(pnow,2), rValue>zValue)
#        print('new Val', logLikZ_s,prior_s, np.round(pnew, 2), rValue>zValue)
        ## UPDATE MODEL ELEMENTS
        if (rValue > zValue):
            self.basicdata.beta = beta_s.copy()
            self.basicdata.obsDot = obsDot_s.copy()
            self.basicdata.w = w_s.copy()
            self.basicdata.randDot = randDot_s.copy()
            self.basicdata.logLikZ = logLikZ_s
            self.basicdata.logLikPigZ = logLikPigZ_s.copy()

    def alpha0_Update(self):
        alpha0_s = np.random.normal(self.basicdata.Alpha0, self.params.searchAlpha0)
        ## LONG ARRAY OF INDIVIDUAL INTERCEPTS FOR OBS DATA
        obsAlpha0_s = alpha0_s[self.basicdata.obsPigIndx]
        ## BETAN DIRECT PREDICTION
        obsBetaN_s = obsAlpha0_s + (self.basicdata.obsAlphaN * self.basicdata.dwrpObs)
        ## DISTANCE TO COAST TIME WRAPPED CAUCHY FOR OBS DATA
        obsCoast_s = obsBetaN_s * self.basicdata.obsLnDCoast
        ## REL PROBABILITY OF PRESENCE FOR ALL OBS DATA
        w_s = self.basicdata.obsDot + obsCoast_s
        logLikZ_s = 0.0
        logLikPigZ_s = np.zeros(self.basicdata.nPigs)
        ## RUN LIKELIHOOD FUNCTION IN BASICDATA
        (logLikZ_s, logLikPigZ_s) = self.basicdata.zLikelihood(alpha0_s, 
            self.basicdata.AlphaN, self.basicdata.a, self.basicdata.b, 
            w_s, self.basicdata.randDot, logLikZ_s, logLikPigZ_s)
#        print('fx lik', logLikZ_s, np.sum(logLikPigZ_s))
        ## BETA PRIORS
        prior = stats.norm.logpdf(self.basicdata.Alpha0, self.basicdata.mu,
            np.sqrt(self.basicdata.sigma))
        prior_s = stats.norm.logpdf(alpha0_s, self.basicdata.mu, 
            np.sqrt(self.basicdata.sigma))
        ##CALCULATE IMPORTANCE RATIO
        pnow = self.basicdata.logLikPigZ + prior
        pnew = logLikPigZ_s + prior_s
        pdiff = pnew - pnow
        rValue = np.ones(self.basicdata.nPigs)
        rValue[pdiff < -12.0] = 0.0
        rmask = (pdiff >= -12.0) & (pdiff < 1.0)
        rValue[rmask] = np.exp(pdiff[rmask])
        zValue = np.random.uniform(0.0, 1.0, size = self.basicdata.nPigs)
        keepA0 = (rValue > zValue)
        ## UPDATE MODEL ELEMENTS
        self.basicdata.Alpha0[keepA0] = alpha0_s[keepA0]
        ## LONG ARRAY OF INDIVIDUAL INTERCEPTS FOR OBS DATA
        self.basicdata.obsAlpha0 = self.basicdata.Alpha0[self.basicdata.obsPigIndx]
        ## LONG ARRAY OF PREDICTED BETAN
        self.basicdata.obsBetaN = (self.basicdata.obsAlpha0 + 
            (self.basicdata.obsAlphaN * self.basicdata.dwrpObs))
        ## DISTANCE TO COAST TIME WRAPPED CAUCHY FOR OBS DATA
        self.basicdata.obsCoast = self.basicdata.obsBetaN * self.basicdata.obsLnDCoast
        ## REL PROBABILITY OF PRESENCE FOR ALL OBS DATA
        self.basicdata.w = self.basicdata.obsDot + self.basicdata.obsCoast
        self.basicdata.logLikPigZ[keepA0] = logLikPigZ_s[keepA0]
        self.basicdata.logLikZ = np.sum(self.basicdata.logLikPigZ)

    def alphaN_Update(self):
        alphaN_s = np.random.normal(self.basicdata.AlphaN, self.params.searchAlphaN)
        ## LONG ARRAY OF COAST BETAN INDIVIDUAL 
        obsAlphaN_s = alphaN_s[self.basicdata.obsPigIndx]
        ## BETAN DIRECT PREDICTION
        obsBetaN_s = self.basicdata.obsAlpha0 + (obsAlphaN_s * self.basicdata.dwrpObs)
        ## DISTANCE TO COAST TIME WRAPPED CAUCHY FOR OBS DATA
        obsCoast_s = obsBetaN_s * self.basicdata.obsLnDCoast 
        ## REL PROBABILITY OF PRESENCE FOR ALL OBS DATA
        w_s = self.basicdata.obsDot + obsCoast_s
        logLikZ_s = 0.0
        logLikPigZ_s = np.zeros(self.basicdata.nPigs)
        ## RUN LIKELIHOOD FUNCTION IN BASICDATA
        (logLikZ_s, logLikPigZ_s) = self.basicdata.zLikelihood(self.basicdata.Alpha0,
            alphaN_s, self.basicdata.a, self.basicdata.b, 
            w_s, self.basicdata.randDot, logLikZ_s, logLikPigZ_s)
#        print('fx lik', logLikZ_s, np.sum(logLikPigZ_s))
        ## BETA PRIORS
        prior = stats.norm.logpdf(self.basicdata.AlphaN, self.basicdata.nu,
            np.sqrt(self.basicdata.epsil))
        prior_s = stats.norm.logpdf(alphaN_s, self.basicdata.nu,
            np.sqrt(self.basicdata.epsil))
        ##CALCULATE IMPORTANCE RATIO
        pnow = self.basicdata.logLikPigZ + prior
        pnew = logLikPigZ_s + prior_s
        pdiff = pnew - pnow
        rValue = np.ones(self.basicdata.nPigs)
        rValue[pdiff < -12.0] = 0.0
        rmask = (pdiff >= -12.0) & (pdiff < 1.0)
        rValue[rmask] = np.exp(pdiff[rmask])
        zValue = np.random.uniform(0.0, 1.0, size = self.basicdata.nPigs)
        keepAN = (rValue > zValue)
        ## UPDATE MODEL ELEMENTS
        self.basicdata.AlphaN[keepAN] = alphaN_s[keepAN]
        ## LONG ARRAY OF betaN FOR OBS DATA
        self.basicdata.obsAlphaN = self.basicdata.AlphaN[self.basicdata.obsPigIndx]
        ## BETAN DIRECT PREDICTION
        self.basicdata.obsBetaN = (self.basicdata.obsAlpha0 + 
            (self.basicdata.obsAlphaN * self.basicdata.dwrpObs))
        ## DISTANCE TO COAST TIME WRAPPED CAUCHY FOR OBS DATA
        self.basicdata.obsCoast = self.basicdata.obsBetaN * self.basicdata.obsLnDCoast 
        ## REL PROBABILITY OF PRESENCE FOR ALL OBS DATA
        self.basicdata.w = self.basicdata.obsDot + self.basicdata.obsCoast
        self.basicdata.logLikPigZ[keepAN] = logLikPigZ_s[keepAN]
        self.basicdata.logLikZ = np.sum(self.basicdata.logLikPigZ)


    def abUpdate(self):
        a_s = np.random.normal(self.basicdata.a, self.params.searchAB[0])
        b_s = np.exp(np.random.normal(np.log(self.basicdata.b), self.params.searchAB[1]))
        ## PROPOSED WRAPPED CAUCHY DENSITY FOR DAYS OF OBSERVED LOCATIONS
        dwrpObs_s = dwrpcauchy(self.basicdata.julRadian, a_s, b_s)
        ## BETAN DIRECT PREDICTION
        obsBetaN_s = self.basicdata.obsAlpha0 + (self.basicdata.obsAlphaN * dwrpObs_s)
        ## DISTANCE TO COAST TIME WRAPPED CAUCHY FOR OBS DATA
        obsCoast_s = obsBetaN_s * self.basicdata.obsLnDCoast 
        ## REL PROBABILITY OF PRESENCE FOR ALL OBS DATA
        w_s = self.basicdata.obsDot + obsCoast_s
        logLikZ_s = 0.0
        logLikPigZ_s = np.zeros(self.basicdata.nPigs)
        ## RUN LIKELIHOOD FUNCTION IN BASICDATA
        (logLikZ_s, logLikPigZ_s) = self.basicdata.zLikelihood(self.basicdata.Alpha0,
            self.basicdata.AlphaN, a_s, b_s, w_s, self.basicdata.randDot, 
            logLikZ_s, logLikPigZ_s)
#        print('fx lik', logLikZ_s, np.sum(logLikPigZ_s))
        ## BETA PRIORS
        prior = (stats.norm.logpdf(self.basicdata.a, self.params.prior_a[0],
            self.params.prior_a[1]) + stats.norm.logpdf(np.log(self.basicdata.b), 
            self.params.prior_b[0], self.params.prior_b[1]))
        prior_s = (stats.norm.logpdf(a_s, self.params.prior_a[0],
            self.params.prior_a[1]) + stats.norm.logpdf(np.log(b_s), 
            self.params.prior_b[0], self.params.prior_b[1]))
        ##CALCULATE IMPORTANCE RATIO
        pnow = self.basicdata.logLikZ + prior
        pnew = logLikZ_s + prior_s
        pdiff = pnew - pnow
        if pdiff > 1.0:
            rValue = 1.0
            zValue = 0.0
        elif pdiff < -12.0:
            rValue = 0.0
            zValue = 1.0
        else:
            rValue = np.exp(pdiff)        # calc importance ratio
            zValue = np.random.uniform(0.0, 1.0, size = None)
#        print('present', self.basicdata.logLikZ, prior,np.round(pnow,2), rValue>zValue)
#        print('new Val', logLikZ_s,prior_s, np.round(pnew, 2), rValue>zValue)
        ## UPDATE MODEL ELEMENTS
        if (rValue > zValue):
            self.basicdata.a = a_s
            self.basicdata.b = b_s
            self.basicdata.dwrpObs = dwrpObs_s.copy()
            self.basicdata.obsBetaN = obsBetaN_s.copy()
            self.basicdata.obsCoast = obsCoast_s.copy()
            self.basicdata.w = w_s.copy()
            self.basicdata.logLikZ = logLikZ_s
            self.basicdata.logLikPigZ = logLikPigZ_s.copy()




    ########            Main mcmc function
    ########
    def mcmcFX(self):

        cc = 0
        for g in range(self.params.ngibbs * self.params.thinrate + self.params.burnin):
#            print('g', g)
            # betas for predicting sigma
            self.beta_UpdateFX()
            self.alpha0_Update()
            self.alphaN_Update()
            self.abUpdate()
            ## UPDATE MU AND SIGMA FOR BETA0
            self.basicdata.mu = meanGibbsUpdate(self.basicdata.Alpha0, 
                self.basicdata.nPigs, self.basicdata.sigma, self.params.muPrior)

            self.basicdata.sigma = varGibbsUpdate(self.params.s1, self.params.s2, 
                self.basicdata.Alpha0, self.basicdata.mu, self.basicdata.nPigs)


            ## UPDATE NU AND EPSILON FOR BETAN
            self.basicdata.nu = meanGibbsUpdate(self.basicdata.AlphaN, 
                self.basicdata.nPigs, self.basicdata.epsil, self.params.priorNu)

            self.basicdata.epsil = varGibbsUpdate(self.params.priorEpsil[0], 
                self.params.priorEpsil[1], 
                self.basicdata.AlphaN, self.basicdata.nu, self.basicdata.nPigs)

#            print('nu', np.round(self.basicdata.nu, 3), 
#                'epsil', np.round(self.basicdata.epsil, 3), 'bn', 
#                np.round(self.basicdata.AlphaN[:6], 3))
#            print('mu', np.round(self.basicdata.mu, 3), 
#                'epsil', np.round(self.basicdata.epsil, 3), 'bn', 
#                np.round(self.basicdata.Alpha0[:6], 3))


            ## POPULATE ARRAYS FOR POST PROCESSING
            if g in self.params.keepseq:
                self.betagibbs[cc] = self.basicdata.beta
                self.alpha0gibbs[cc] = self.basicdata.Alpha0
                self.alphaNgibbs[cc] = self.basicdata.AlphaN
                self.agibbs[cc] = self.basicdata.a
                self.bgibbs[cc] = self.basicdata.b
                self.mugibbs[cc] = self.basicdata.mu
                self.sigmagibbs[cc]= self.basicdata.sigma
                self.nugibbs[cc] = self.basicdata.nu
                self.epsilgibbs[cc]= self.basicdata.epsil
                self.Dgibbs[cc] = -2.0 * self.basicdata.logLikZ
                cc = cc + 1





