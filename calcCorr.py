#!/usr/bin/env python


from paramsMod2 import ModelParams
import numpy as np
import pylab as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
import os
from scipy import stats

class CorrCalculation(object):
    def __init__(self, gibbsobj, basicobj, params, pigpath):

        self.basicdata = basicobj


        self.makeData()
        self.corrLoop()

    def makeData(self):
        self.names = np.array(['PTus', 'MnDevN', 'D2Riv', 'PFor',
            'PBog', 'DCoast'])
        self.ncov = len(self.names)

        print('xobs', np.shape(self.basicdata.xObs), np.shape(self.basicdata.xRand))

        xDat = np.vstack((self.basicdata.xObs, self.basicdata.xRand))
        dCoast = np.append(self.basicdata.obsLnDCoast, self.basicdata.randLnDCoast)

        self.xDatCor= np.hstack([xDat, np.expand_dims(dCoast, 1)])

        print('xdat', self.xDatCor[:10])

    def corrLoop(self):
        for i in range(self.ncov):
            cov_i = self.xDatCor[:, i]
            name_i = self.names[i]
            for j in range((i + 1), self.ncov):
                name_j = self.names[j]
                corr_ij = stats.pearsonr(cov_i, self.xDatCor[:, j])
                print('Corr ' + name_i + ':' + name_j, corr_ij)

def main():
    params = ModelParams()
    print('########################')
    print('########################')
    print('###')
    print('#    Model 2')
    print('###')
    print('########################')
    print('########################')

    # paths and data to read in

    pigpath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', default = '.'),
            'AuckIsl', 'Results', 'mod2Results')


    fileobj = open(params.mcmcFname, 'rb')
    gibbsobj = pickle.load(fileobj)
    fileobj.close()

    fileobj = open(params.basicdataFname, 'rb')
    basicobj = pickle.load(fileobj)
    fileobj.close()

    print('gibbsResults Name:', params.mcmcFname)    

    corrobj = CorrCalculation(gibbsobj, basicobj, params, pigpath)


if __name__ == '__main__':
    main()


