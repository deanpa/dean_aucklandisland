#!/usr/bin/env python


import os
import pickle
import numpy as np
from scipy import stats
import datetime
from osgeo import osr
import pylab as P
from scipy.stats.mstats import mquantiles



def decodeBytes(byteArray, nArray):
    """
    ## loop to decode byte to string
    """
    strArray = str(byteArray[0], 'utf-8')
    for i in range(1, nArray):
        strArr_i = str(byteArray[i], 'utf-8')
        strArray = np.append(strArray, strArr_i)
    return(strArray)

def formatDate(inArray):
    n = len(inArray)
    outArray = np.empty(n, dtype = datetime)
    for i in range(n):
        date_i = inArray[i]
        if date_i != '':
            outArray[i] = datetime.strptime(date_i, '%d/%m/%Y').date()
        else:
            outArray[i] = datetime(1999, 1, 1).date()
    return(outArray)


def distFX(x1, y1, x2, y2):
    """
    calc distance matrix between 2 sets of points
    resulting matrix dimensions = (len(x1), len(x2))
    """
    deltaX = np.add.outer(x1, -x2)
    deltaX_sq = deltaX**2
    deltaY = np.add.outer(y1, -y2)
    deltaY_sq = deltaY**2
    dist_2D = np.sqrt(deltaX_sq + deltaY_sq)
    return dist_2D

 

class ManipData():
    def __init__(self, argosFname, argosOutFname):
        """
        Object to read in  data
        """
        #############################################
        ############ Run basicdata functions
        self.readData(argosFname)
        self.getDateTime()
        self.cullData()
        self.assessVelocity()
###        self.plotVelocity()

###        self.writeToFileFX(argosOutFname)

        ############## End run of basicdata functions
        #############################################

    def readData(self, argosFname):
        """
        ## read and transfer argos and trapping data to manipdata class
        """
        argosDat = np.genfromtxt(argosFname,  delimiter=',', names=True,
            dtype=['f8', 'f8', 'i8', 'f8', 'f8', 'i8', 'S10', 'S10', 'i8', 'S32'])
        self.x = argosDat['X']
        self.y = argosDat['Y']
        self.collar = argosDat['Collar']
        self.msgDate = argosDat['MsgDate']
        self.locDate = argosDat['LocDate']
        self.locTime = argosDat['LocTime']
        self.locQuality = argosDat['LocQuality']
        self.nArgos = len(self.x)
        ## convert bytes to string and format Dates
        self.dateTmp = decodeBytes(self.locDate, self.nArgos)
        self.timeTmp = decodeBytes(self.locTime, self.nArgos)
        self.uPig = np.unique(self.collar)
        self.nUPig = len(self.uPig)

        print('n pigs', self.nUPig, 'u pigs', self.uPig)

#        print('x', self.x[:20], self.collar[:5], 'msgDate', self.msgDate[:5],
#            'locDate', self.locDate[:5], 'locquality', self.locQuality[:10])


    def getDateTime(self):
        self.dateTimeFull = []
        for i in range(self.nArgos):
            date_i = datetime.datetime.strptime(self.dateTmp[i], '%d/%m/%y')
            time_i = datetime.datetime.strptime(self.timeTmp[i], '%H:%M:%S').time()

            self.dateTimeFull.append(datetime.datetime.combine(date_i, time_i))
#            if i < 30:
#                print('i', i, 'date', date_i, 'mon', date_i.month)
        self.dateTimeFull = np.array(self.dateTimeFull)

    def cullData(self):
        ## MIN TIME IN HOURS
        self.minHours = 2.0
        ## VELOCITY CUT OFF
        self.maxVelocity = 15.0
        keepMask = np.ones(self.nArgos, dtype = bool)
        maskNotJ = np.ones(self.nArgos, dtype = bool)
        ## START COUNTER
        cc = 0
        for i in range(self.nUPig):
            ## MASK FOR PIG I
            indMask = self.collar == self.uPig[i]
            ## ARRAY OF DATES FOR PIG I
            dt_i = self.dateTimeFull[indMask]
            ## NUMBER OF DATE ENTRIES FOR PIG I
            nDT = len(dt_i)
            ## FIRST OR EARLIEST DATE
            minDT = np.min(dt_i)
            ## LOOP THRU ALL DATES AND KEEP THOSE > MINHOURS.
            for j in range(nDT):
                ## RESET PREVIOUS ENTRY TO A KEEP
                if cc > 0:
                    maskNotJ[cc - 1] = True
                ## COUNTER
                cc += 1
                ## DATE J FOR ASSESSMENT
                dt_ij = dt_i[j]
                ## IF FIRST DATE, CONTINUE
                if dt_ij == minDT:
                    continue
                ## MASK OF ALL DATES EXCEPT J - SELECT OUT J
                maskNotJ[cc - 1] = False
                ## MASK OF PRECEDING AND SAME TIME AS J (IN CASE OF REPEATS IN DATA)
                maskPosDiff = dt_ij >= self.dateTimeFull     # dt_i
                ## MASK NOT J AND PRECEDING OR SAME TIME AS J
                maskNotJPossDiff = maskNotJ & maskPosDiff & keepMask & indMask
                ## TIME TO CLOSEST PREVIOUS FIX
                maxTime = np.max(self.dateTimeFull[maskNotJPossDiff])
                minDiff = dt_ij - maxTime
                ## IF TIME TO PREVIOUS IS LESS THAN MINHOURS, DONT KEEP
                if (minDiff <= datetime.timedelta(seconds = (self.minHours *3600))):
                    keepMask[cc - 1] = False
#                if i == 0:
#                    print('cc', cc-1, 'mindiff', minDiff, 'mask', keepMask[cc-1])
            print('collar', self.uPig[i], 'N Fixes', np.sum(keepMask[indMask]))
        ## KEEP DATA WITH TIME INTERVAL > TIME HOUR THRESHOLD
        self.x = self.x[keepMask]
        self.y = self.y[keepMask]
        self.collar = self.collar[keepMask]
        self.dateTimeFull = self.dateTimeFull[keepMask]
        self.locDate = self.locDate[keepMask]
        self.locQuality = self.locQuality[keepMask]
        self.nArgos = len(self.x)

    def assessVelocity(self):
        self.cutVelocity = 1610.0
        self.Velocity = []
        self.Distance = []
        self.X = []
        self.Y = []
        self.TimeDate = []
        self.Quality = []
        self.Collar = []
        allVelocity = []
#        self.Velocity = np.zeros(self.nArgos)
#        self.Distance = np.zeros(self.nArgos)
#        self.X = np.zeros(self.nArgos)
#        self.Y = np.zeros(self.nArgos)
#        self.TimeDate = np.zeros(self.nArgos, dtype = datetime.datetime)
#        self.Quality = np.zeros(self.nArgos, dtype = int)
#        self.Collar = np.zeros(self.nArgos, dtype = int)
        ## START COUNTER
        cc = 0
        for i in range(self.nUPig):
            ## MASK FOR PIG I
            indMask = self.collar == self.uPig[i]
            ## ARRAY OF DATES (SORTED) AND LOCATIONS FOR PIG I
            dt_i = self.dateTimeFull[indMask] 
            dtSorted = np.sort(dt_i)  
            x_i = self.x[indMask]
            y_i = self.y[indMask]
            collar_i = self.collar[indMask]
            qual_i = self.locQuality[indMask]
            ## NUMBER OF DATE ENTRIES FOR PIG I
            nDT = len(dt_i)
            ## LOOP THRU ALL DATES AND KEEP THOSE > MINHOURS.
            for j in range(nDT):
                dt_j = dtSorted[j]
                mask_dt = dt_i == dt_j
                x_j = x_i[mask_dt]
                y_j = y_i[mask_dt]
#                self.Quality[cc] = qual_i[mask_dt]
#                self.TimeDate[cc] = dt_j
#                self.Y[cc] = y_j
#                self.X[cc] = x_j
#                self.Collar[cc] = self.uPig[i]
                if j == 0:
                    self.Velocity.append(0.0)
                    self.Distance.append(0.0)
                    self.Quality.append(qual_i[mask_dt])
                    self.TimeDate.append(dt_j)
                    self.Y.append(y_j)
                    self.X.append(x_j)
                    self.Collar.append(self.uPig[i])

                else:
#                    dist = distFX(x_j, self.X[cc - 1], y_j, self.Y[cc - 1]) / 1000
                    dx2 = (x_j - self.X[cc - 1])**2
                    dy2 = (y_j - self.Y[cc - 1])**2
                    dist = np.sqrt(dx2 + dy2).item()
                    timeDiff = (dt_j - self.TimeDate[cc - 1]) 
                    hourDiff = (timeDiff.days / 24.0) + (timeDiff.seconds / 3600.0) 
                    vel_j = (dist / hourDiff)
                    allVelocity.append(vel_j)
                    if vel_j < self.cutVelocity:
                        self.Velocity.append(vel_j)
                        self.Distance.append(dist)
                        self.Quality.append(qual_i[mask_dt])
                        self.TimeDate.append(dt_j)
                        self.Y.append(y_j)
                        self.X.append(x_j)
                        self.Collar.append(self.uPig[i])
                        cc += 1
#                    else:
#                        print('i', self.uPig[i], 'j', j, 'dt', dt_j, 'hour', np.round(hourDiff,2), 
#                            'dist', np.round(dist,2),'vel', np.round(vel_j, 2), 
#                            'x', np.round(x_j,2), 'y', np.round(y_j,2), 
#                            'x-1', np.round(self.X[cc - 1],0), 
#                            'y-1', np.round(self.Y[cc - 1],0),
#                            'date - 1', self.TimeDate[cc-1])
        self.Velocity = np.array(self.Velocity)
        self.Distance = np.array(self.Distance)
        self.Quality = np.array(self.Quality)
        self.TimeDate = np.array(self.TimeDate)
        self.Y = np.array(self.Y)
        self.X = np.array(self.X)
        self.Collar = np.array(self.Collar)
        self.nArgos = len(self.X)
        allVelocity = np.array(allVelocity)
        quantVel = mquantiles(allVelocity[allVelocity >0], 
                prob = [0.0275, 0.5, 0.9725, 0.975, 0.99])
        print('quantVel', quantVel, 'max', np.max(allVelocity),
            'n Removed', -len(self.Velocity) + len(allVelocity))

    def plotVelocity(self):
        quants = mquantiles(self.Velocity[self.Velocity >0], 
                prob = [0.0275, 0.5, 0.975, 0.99])
        print('Velocity Quants', quants, 'Max', np.max(self.Velocity))
        P.figure(figsize = (11,6))
        P.subplot(1,2,1)
        P.hist(self.Velocity[self.Velocity > 0], bins = 60)
        P.axvline(x = quants[1], color = 'k')
        P.axvline(x = quants[2], color = 'k')
        P.ylabel('Frequency')
        P.xlabel('Velocity ' + r'$(m \cdot hour^{-1})$')
        P.xlim(0, 5000)

        quants = mquantiles(self.Distance[self.Distance >0], 
                prob = [0.0275, 0.5, 0.975, 0.99])
        print('Distance Quants', quants, 'Max', np.max(self.Distance))
        P.subplot(1,2,2)
        P.hist(self.Distance[self.Distance > 0], bins = 120)
        P.axvline(x = quants[1], color = 'k')
        P.axvline(x = quants[2], color = 'k')
        P.ylabel('Frequency')
        P.xlabel('Distance between moves ($m$)')
        P.xlim(0, 5000)
        P.savefig('VelocityDistanceHist.png', format='png')
        P.show()




    def writeToFileFX(self, argosOutFname):
        """
        # Write result table to file
        """
        # create new structured array with columns of different types
        structured = np.empty((self.nArgos,), dtype=[('x', np.float), ('y', np.float), 
                    ('collar', 'i8'), ('timeDate', 'U32'), ('locQuality', 'i8')])
        # copy data over
        structured['x'] = self.X.flatten()
        structured['y'] = self.Y.flatten()
        structured['collar'] = self.Collar
        structured['timeDate'] = self.TimeDate
        structured['locQuality'] = self.Quality.flatten()
        fmts = ['%.4f', '%.4f', '%i', '%s', '%i']
        np.savetxt(argosOutFname, structured, comments = '', delimiter = ',', fmt = fmts,
                    header='x, y, collar, timeDate, locQuality')













######################
# Main function
def main():
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', 
        default = '.'), 'AuckIsl')
    outputDataPath = os.path.join(os.getenv('AUCKLANDISLANDPROJDIR', 
        default = '.'), 'AuckIsl')


    ## set Data names
    argosFname = os.path.join(inputDataPath, 'AllDataFromARGOS_XY.csv')
    argosOutFname = os.path.join(inputDataPath, 'moveData.csv')

    manipdata = ManipData(argosFname, argosOutFname)



if __name__ == '__main__':
    main()





