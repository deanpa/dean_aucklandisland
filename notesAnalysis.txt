Notes on analysis


6 January 2021
###################
pseudoAbsentPigs.py 

Used this script to get 50 random points for each observed point (class SpatialData). 
This class wrote a data file with the xy coord, time and collar corresponding to the 
observed data ("pseudoAbsentData.csv"). These new data were generated using 
"pigs_cullHighVelocity_2.csv." The random points were put into the mcp shapefile 
"mcp100_NoIslands.shp".
 
Then got the covariate data from rasters (class Covariate data). Input into this class
was "pseudoAbsentData.csv" from above, and the directory with all the covariate data.
 Wrote to directory the data file "pseudoCovar.csv". 

####################

However, I noticed that there was no covariate for distance to coast. Upon 
further reflection, I decided that using the original random data with 1000 points
in each mcp was probably a better approach - maybe. This still needs some thought and we
can play with the two approaches. 

#####################

I created a dCoast.img raster in QGIS. This was done by converting the island polygon 
shapefile to a line shapefile ("NorthAuck_Lines.shp"). I then converted the line 
shapefile to a raster (2m resol; too fine). I then used the proximity tool in QGIS to 
create a distance to coast raster. 

#####################
pointIntercept.py

I used this script to extract the data for the observed and the random data. The random
data used 'pigs_1000RP_XY2.csv' to make the final file 'pigs_rand1000.csv'. The 
pigs_1000RP_XY2.csv has the x y coordinates added, which were not present in the 
'pigs_1000RP2.csv'. 

I used pigs_cullHighVelocity_2.csv to add on the dCoast.img data. The resulting file is
'pigs_observed.csv'

######################

For the analysis, use 'pigs_observed.csv' and 'pigs_rand1000.csv'. 

######################
######################
######################
MODELS
######################
Model 1: ptus, dRiv, pfor, DevN; at 200 m radius

Model 2: ptus, dRiv, pfor, DevN, pBog; at 200 m radius

Model 3: ptus, dRiv, pfor, DevN; at 500 m radius

Model 4: ptus, dRiv, pfor, DevN, pBog; at 500 m radius

## MODELS WITH NO COLLINEARITY
Model 5: ptus, dRiv, DevN; at 200 m radius

Model 6: dRiv, DevN, pFor; at 200 m radius

Model 7: Ptus, dRiv, DevN, pBog; at 200 m radius

Model 8: dRiv, DevN, pFor, pBog; at 200 m radius

Model 9: ptus, dRiv, DevN; at 500 m radius

Model 10: dRiv, DevN, pFor; at 500 m radius

Model 11: Ptus, dRiv, DevN, pBog; at 500 m radius

Model 12: dRiv, DevN, pFor, pBog; at 500 m radius





